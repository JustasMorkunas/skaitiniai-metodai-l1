﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pvz1
{
    class SaknuTikslinimas
    {
        double minX;
        double maxX;
        Func<double, double> funkcija;
        RichTextBox isvedimas;

        public SaknuTikslinimas(double minX, double maxX, Func<double, double> funkcija, RichTextBox isvedimas) {
            this.minX = minX;
            this.maxX = maxX;
            this.funkcija = funkcija;
            this.isvedimas = isvedimas;
        }

        private void isvestiRezultata(int iteracija, double x, double y) {
            isvedimas.AppendText(isvedimoTekstas(iteracija, x, y));
        }

        private string isvedimoTekstas(int iteracija, double x, double y) {
            return String.Format("{0,15} {1,15:f10} {2,15:f10}\n", iteracija, x, y);
        }


        public double tikslintiStyguMetodu(ref int iteracija) {
            double xn = minX;
            double xn1 = maxX;

            double k = Math.Abs(funkcija(xn) / funkcija(xn1));

            double xMid = (xn + (k * xn1)) / (1.0 + k);

            while (Math.Abs(funkcija(xMid)) > Parametrai.NORIMAS_TIKSLUMAS && iteracija <= Parametrai.MAKSIMALUS_ITERACIJU_SKAICIUS) {

                isvestiRezultata(iteracija, xMid, funkcija(xMid));

                k = (xMid - xn) / (xn1 - xMid);

                if (Math.Sign(funkcija(xMid)) == Math.Sign(funkcija(xn)))
                {
                    xn = xMid;
                }
                else {
                    xn1 = xMid;
                }

                xMid = (xn + (k * xn1)) / (1.0 + k);
                iteracija++;
            }
            isvestiRezultata(iteracija, xMid, funkcija(xMid));

            return xMid;
        }

        // ---------------------- KIRSTINIU METODAS ---------------------------------
        public double tikslintiKirstiniuMetodu(ref int iteracija) {
            double xn = minX;
            double xn1 = maxX;

            while (Math.Abs(funkcija(xn1)) > Parametrai.NORIMAS_TIKSLUMAS && iteracija <= Parametrai.MAKSIMALUS_ITERACIJU_SKAICIUS)
            {
                isvestiRezultata(iteracija, xn1, funkcija(xn1));
                double laik = xn1;

                xn1 = xn - (Parametrai.KIRSTINIU_METODO_ANTI_KONVERGAVIMO_KOEFICIENTAS * ((funkcija(xn) - funkcija(xn1)) / (xn - xn1)) * funkcija(xn));
                xn = laik;

                iteracija++;
            }

            isvestiRezultata(iteracija, xn1, funkcija(xn1));
            return xn1;
        }

        // ---------------------- SKENAVIMO SU MAZEJANCIU INTERVALU METODAS ---------------------------------

        public double tikslintiSkenavimoSuMazejanciuIntervaluMetodu(ref int iteracijos) {
            
            double x1 = minX;
            double x2 = maxX;

            return skenuoti(x1, x2, ref iteracijos);
        }

        private double skenuoti(double x1, double x2, ref int iteracija) {

            if (Math.Abs(funkcija(x1)) < Parametrai.NORIMAS_TIKSLUMAS) {
                isvestiRezultata(iteracija, x1, funkcija(x1));
                return x1;
            }


            if (iteracija > Parametrai.MAKSIMALUS_ITERACIJU_SKAICIUS) {
                isvestiRezultata(iteracija, x1, funkcija(x1));
                return x1;
            }
                
            double xmid = (x1 + x2) / 2;
            iteracija++;

            if (Math.Sign(funkcija(x1)) != Math.Sign(funkcija(xmid)))
            {
                isvestiRezultata(iteracija, x1, funkcija(x1));
                return skenuoti(x1, xmid, ref iteracija);
            }
            else if (Math.Sign(funkcija(xmid)) != Math.Sign(funkcija(x2))) {
                isvestiRezultata(iteracija, xmid, funkcija(xmid));
                return skenuoti(xmid, x2, ref iteracija);   
            }

            isvestiRezultata(iteracija, xmid, funkcija(xmid));
            return xmid;
        } 


    }
}
