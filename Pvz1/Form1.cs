﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Factorization;

namespace Pvz1
{
    public partial class Form1 : Form
    {
        List<Timer> Timerlist = new List<Timer>();

        public Form1()
        {
            InitializeComponent();
            Initialize();
        }

        // ---------------------------------------------- SKAITINIU METODU REALIZACIJOS ----------------------------------------------

        float x1, x2, xtemp; // izoliacijos intervalo pradžia ir galas, vidurio taškas
        int N = 1000; // maksimalus iteracijų skaičius
        int iii; // iteracijos numeris

        Series Fx, X1X2, XMid; // naudojama atvaizduoti f-jai, šaknų rėžiams ir vidiniams taškams

        // Funkcijos F(x) koeficientai ir laipsniai;
        List<double> koeficientai = new List<double> { 0.88, -1.44, -5.33, 7.35, 0.83 };
        List<int> laipsniai = new List<int> { 4, 3, 2, 1, 0 };

        /// <summary>
        /// Sprendžiama lygtis F(x) = 0
        /// </summary>
        /// <param name="x">funkcijos argumentas</param>
        /// <returns></returns>
        public double F(double x)
        {
            return (double)((0.88 * Math.Pow(x, 4)) - (1.44 * Math.Pow(x, 3)) - (5.33 * Math.Pow(x, 2)) + (7.35 * x) + 0.83);
        }

        /// <summary>
        /// Funckcijos G(x) mygtuko paspaudimas.
        /// </summary>
        public double G(double x) {
            return (double)((1.9 * x * Math.Sin(x)) - Math.Pow(((x / 1.5) - 3), 2));
        }

        public double V(double x) {
            return (double)((15 * Math.Pow(Math.E, (-3.0 * x) / 0.45)) + (((0.45 * 9.8) / x)) * ((Math.Pow(Math.E, (-3.0 * x) / 0.45)) - 1) - 49);
        }

        private List<double[]> vykdytiSkenavima(double xMin, double xMax, Func<double, double> func) {
            List<double[]> intervalai = new List<double[]>();
            for (double zing = xMin; zing + Parametrai.SKENAVIMO_INTERVALAS <= xMax; zing += Parametrai.SKENAVIMO_INTERVALAS) {
                if (Math.Sign(func(zing)) != Math.Sign(func(zing + Parametrai.SKENAVIMO_INTERVALAS))) {
                    intervalai.Add(new double[] { zing, zing + Parametrai.SKENAVIMO_INTERVALAS });
                }
            }

            return intervalai;
        }


        // Mygtukas "Pusiaukirtos metodas" - ieškoma šaknies, ir vizualizuojamas paieškos procesas
        private void button3_Click(object sender, EventArgs e)
        {
            ClearForm(); // išvalomi programos duomenys

            ReziuSkaiciavimas reziuSkaiciuokle = new ReziuSkaiciavimas(koeficientai, laipsniai);
            double[] reziai = reziuSkaiciuokle.tikslesnisSaknuIntervalas();

            PreparareForm(-6, 4, -10, 10);
            x1 = 2; // izoliacijos intervalo pradžia
            x2 = 5; // izoliacijos intervalo galas
            iii = 0; // iteraciju skaičius
            richTextBox1.AppendText("Iteracija         x            F(x)        x1          x2          F(x1)         F(x2)       \n");
            // Nubraižoma f-ja, kuriai ieskome saknies
            Fx = chart1.Series.Add("F(x)");
            Fx.ChartType = SeriesChartType.Line;
            double x = -4;
            for (int i = 0; i < 100; i++)
            {
                Fx.Points.AddXY(x, F(x)); x = x + 0.1;
            }
            Fx.BorderWidth = 3;

            X1X2 = chart1.Series.Add("X1X2");
            X1X2.MarkerStyle = MarkerStyle.Circle;
            X1X2.MarkerSize = 8;
            X1X2.ChartType = SeriesChartType.Line;


            XMid = chart1.Series.Add("XMid");
            XMid.MarkerStyle = MarkerStyle.Circle;
            XMid.MarkerSize = 8;

            timer2.Enabled = true;
            timer2.Interval = 500; // timer2 intervalas milisekundemis
            timer2.Start();
        }


        /// <summary>
        /// timer2 iteracijoje atliekami veiksmai
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            xtemp = (x1 + x2) / 2; // apskaiciuojamas vidurinis taskas


            if (Math.Abs(F(xtemp)) > 1e-6 & iii <= N)
            // tikrinama salyga, ar funkcijos absoliuti reiksme daugiau uz nustatyta (norima) 
            // tiksluma ir nevirsytas maksimalus iteraciju skaicius
            {
                X1X2.Points.Clear();
                XMid.Points.Clear();

                X1X2.Points.AddXY(x1, 0);
                X1X2.Points.AddXY(x2, 0);
                XMid.Points.AddXY(xtemp, 0);

                richTextBox1.AppendText(String.Format(" {0,6:d}   {1,12:f7}  {2,12:f7} {3,12:f7} {4,12:f7} {5,12:f7} {6,12:f7}\n",
                    iii, xtemp, F(xtemp), x1, x2, F(x1), F(x2)));
                if (Math.Sign((double)F(x1)) != Math.Sign((double)F(xtemp)))
                {
                    x2 = xtemp;
                }
                else
                {
                    x1 = xtemp;
                }
                iii = iii + 1;

            }
            else
            // skaiciavimai stabdomi
            {
                richTextBox1.AppendText("Skaičiavimai baigti");
                timer2.Stop();
            }
        }


        // ---------------------------------------------- GRAFINE VARTOTOJO SASAJA ----------------------------------------------

        List<PointF> data = new List<PointF>();
        Series S1;

        /// <summary>
        /// Parametrinis interpoliavimas
        /// </summary>
        private void button5_Click(object sender, EventArgs e)
        {
            ClearForm(); // išvalomi programos duomenys
            PreparareForm(-10, 10, -10, 10);
            data.Clear();
            // apskaičiuojamos funkcijos reikšmės
            for (int i = 0; i < 400; i++)
            {
                float x = i / 50f * (float)(Math.Sin(2 * i / 10f));
                float y = i / 50f * (float)(Math.Sin(i / 10f));
                data.Add(new PointF(x, y));
            }
            S1 = chart1.Series.Add("S1");
            S1.BorderWidth = 9;
            S1.ChartType = SeriesChartType.Line;

            timer3.Enabled = true;
            timer3.Interval = 15;
            timer3.Start();
        }


        private void timer3_Tick(object sender, EventArgs e)
        {
            Series S1 = chart1.Series[0];
            int pointsSoFar = S1.Points.Count;
            if (pointsSoFar < data.Count)
            {
                S1.Points.AddXY(data[pointsSoFar].X, data[pointsSoFar].Y);
            }
            else
            {
                timer1.Stop();
            }
        }

        /// <summary>
        /// Reziu skaiciavimas mygtuko paspaudimas.
        /// </summary>
        private void Button6_Click(object sender, EventArgs e)
        {
            ClearForm();

            ReziuSkaiciavimas reziuSkiaciuokle = new ReziuSkaiciavimas(koeficientai, laipsniai);
            double[] grubusReziai = reziuSkiaciuokle.grubusSaknuIntervalas();
            double[] tikslesniReziai = reziuSkiaciuokle.tikslesnisSaknuIntervalas();
            float formMinX = (float) (((int) reziuSkiaciuokle.RezisMinX(grubusReziai)) - 1);
            float formMaxX = (float) (((int) reziuSkiaciuokle.RezisMaxX(grubusReziai)) + 1);

            PreparareForm(formMinX, formMaxX, -10, 10);
            data.Clear();
            drawFxFunction(formMinX, formMaxX);

            Series grubus = new Series("Grubus");
            grubus.MarkerSize = 8;
            grubus.MarkerStyle = MarkerStyle.Circle;
            grubus.ChartType = SeriesChartType.Line;
            grubus.Color = Color.Red;
            grubus.Points.AddXY(reziuSkiaciuokle.RezisMinX(grubusReziai), 0);
            grubus.Points.AddXY(reziuSkiaciuokle.RezisMaxX(grubusReziai), 0);
            chart1.Series.Add(grubus);

            Series tikslesnis = new Series("Tikslesnis");
            tikslesnis.MarkerSize = 8;
            tikslesnis.MarkerStyle = MarkerStyle.Circle;
            tikslesnis.Color = Color.Violet;
            tikslesnis.ChartType = SeriesChartType.Line;
            tikslesnis.Points.AddXY(reziuSkiaciuokle.RezisMinX(tikslesniReziai), 0);
            tikslesnis.Points.AddXY(reziuSkiaciuokle.RezisMaxX(tikslesniReziai), 0);
            chart1.Series.Add(tikslesnis);

            richTextBox1.AppendText(String.Format("Grubus šaknų intervalo nustatymas: apatinis: {0,0:f6}, viršutinis {1,0:f6}\n", reziuSkiaciuokle.RezisMinX(grubusReziai), reziuSkiaciuokle.RezisMaxX(grubusReziai)));
            richTextBox1.AppendText(String.Format("Tikslesnis šaknų intervalo nustatymas: apatinis: {0,0:f6}, viršutinis {1,0:f6}\n", reziuSkiaciuokle.RezisMinX(tikslesniReziai), reziuSkiaciuokle.RezisMaxX(tikslesniReziai)));
        }

        private void drawFxFunction(float formMinX, float formMaxX) {
            chart1.Series.Clear();
            Fx = chart1.Series.Add("F(x)");
            Fx.ChartType = SeriesChartType.Line;
            double x = formMinX;
            for (x = formMinX; x < formMaxX; x = x + 0.1)
            {
                Fx.Points.AddXY(x, F(x)); x = x + 0.1;
            }
            Fx.BorderWidth = 3;
        }

        private void drawGxFunction(double minX, double maxX) {
            Series Gx = chart1.Series.Add("G(x)");
            Gx.ChartType = SeriesChartType.Line;
            for (double x = minX; x < maxX; x = x + 0.1)
            {
                Gx.Points.AddXY(x, G(x));
            }
            Gx.BorderWidth = 3;
        }

        /// <summary>
        /// F(x) mygtuko paspaudimas
        /// </summary>
        private void Button8_Click(object sender, EventArgs e)
        {
            PreparareForm(-6, 6, -10, 10);
            data.Clear();
            drawFxFunction(-6, 6);
        }

        /// <summary>
        /// G(x) mygtuko paspaudimas
        /// </summary>
        private void Button9_Click(object sender, EventArgs e)
        {
            PreparareForm(-10, 10, -10, 10);
            data.Clear();
            chart1.Series.Clear();

            drawGxFunction(-10, 10);
        }

        /// <summary>
        /// Stygu metodas F(x) mygtuko paspaudimas.
        /// </summary>
        private void Button7_Click(object sender, EventArgs e)
        {
            ReziuSkaiciavimas reziuSkiaciuokle = new ReziuSkaiciavimas(koeficientai, laipsniai);
            double[] tikslesniReziai = reziuSkiaciuokle.tikslesnisSaknuIntervalas();
            float formMinX = (float)(((int)reziuSkiaciuokle.RezisMinX(tikslesniReziai)) - 1);
            float formMaxX = (float)(((int)reziuSkiaciuokle.RezisMaxX(tikslesniReziai)) + 1);

            PreparareForm(formMinX, formMaxX, -10, 10);
            data.Clear();
            drawFxFunction(formMinX, formMaxX);

            List<double[]> saknuIntervalai = vykdytiSkenavima(reziuSkiaciuokle.RezisMinX(tikslesniReziai), reziuSkiaciuokle.RezisMaxX(tikslesniReziai), F);
            Series saknys = new Series("Šaknys");
            saknys.Color = Color.Red;
            saknys.ChartType = SeriesChartType.Point;

            richTextBox1.Clear();
            richTextBox1.AppendText(String.Format("{0,15} {1,15} {2,15}\n", "Iteracija", "X reiksme", "Y reiksme"));
            int iteracijos = 1;

            foreach (double[] intervalas in saknuIntervalai) {
                SaknuTikslinimas saknuTikslinimas = new SaknuTikslinimas(intervalas[0], intervalas[1], F, richTextBox1);
                double x = saknuTikslinimas.tikslintiStyguMetodu(ref iteracijos);

                saknys.Points.AddXY(x, F(x));
            }

            chart1.Series.Add(saknys);
        }

        /// <summary>
        /// Stygu metodas G(x) mygtuko paspaudimas.
        /// </summary>
        private void Button10_Click(object sender, EventArgs e)
        {
            ReziuSkaiciavimas reziuSkiaciuokle = new ReziuSkaiciavimas(koeficientai, laipsniai);
            double[] tikslesniReziai = reziuSkiaciuokle.tikslesnisSaknuIntervalas();
            float formMinX = (float)(((int)reziuSkiaciuokle.RezisMinX(tikslesniReziai)) - 1);
            float formMaxX = (float)(((int)reziuSkiaciuokle.RezisMaxX(tikslesniReziai)) + 1);

            PreparareForm(formMinX, formMaxX, -10, 10);
            data.Clear();
            chart1.Series.Clear();

            drawGxFunction(-10, 10);

            List<double[]> saknuIntervalai = vykdytiSkenavima(reziuSkiaciuokle.RezisMinX(tikslesniReziai), reziuSkiaciuokle.RezisMaxX(tikslesniReziai), G);
            Series saknys = new Series("Šaknys");
            saknys.Color = Color.Red;
            saknys.ChartType = SeriesChartType.Point;

            richTextBox1.Clear();
            richTextBox1.AppendText(String.Format("{0,15} {1,15} {2,15}\n", "Iteracija", "X reiksme", "Y reiksme"));
            int iteracijos = 1;

            foreach (double[] intervalas in saknuIntervalai)
            {
                SaknuTikslinimas saknuTikslinimas = new SaknuTikslinimas(intervalas[0], intervalas[1], G, richTextBox1);
                double x = saknuTikslinimas.tikslintiStyguMetodu(ref iteracijos);

                saknys.Points.AddXY(x, G(x));
            }

            chart1.Series.Add(saknys);
        }

        /// <summary>
        /// Kirstiniu metodas F(x) mygtuko paspaudimas.
        /// </summary>
        private void Button11_Click(object sender, EventArgs e)
        {
            ReziuSkaiciavimas reziuSkiaciuokle = new ReziuSkaiciavimas(koeficientai, laipsniai);
            double[] tikslesniReziai = reziuSkiaciuokle.tikslesnisSaknuIntervalas();
            float formMinX = (float)(((int)reziuSkiaciuokle.RezisMinX(tikslesniReziai)) - 1);
            float formMaxX = (float)(((int)reziuSkiaciuokle.RezisMaxX(tikslesniReziai)) + 1);

            PreparareForm(formMinX, formMaxX, -10, 10);
            data.Clear();
            drawFxFunction(formMinX, formMaxX);

            List<double[]> saknuIntervalai = vykdytiSkenavima(reziuSkiaciuokle.RezisMinX(tikslesniReziai), reziuSkiaciuokle.RezisMaxX(tikslesniReziai), F);
            Series saknys = new Series("Šaknys");
            saknys.Color = Color.Red;
            saknys.ChartType = SeriesChartType.Point;

            richTextBox1.Clear();
            richTextBox1.AppendText(String.Format("{0,15} {1,15} {2,15}\n", "Iteracija", "X reiksme", "Y reiksme"));
            int iteracijos = 1;

            foreach (double[] intervalas in saknuIntervalai)
            {
                SaknuTikslinimas saknuTikslinimas = new SaknuTikslinimas(intervalas[0], intervalas[1], F, richTextBox1);
                double x = saknuTikslinimas.tikslintiKirstiniuMetodu(ref iteracijos);

                saknys.Points.AddXY(x, F(x));
            }

            chart1.Series.Add(saknys);
        }

        /// <summary>
        /// Kirstiniu metodas G(x) mygtuko paspaudimas.
        /// </summary>
        private void Button12_Click(object sender, EventArgs e)
        {
            ReziuSkaiciavimas reziuSkiaciuokle = new ReziuSkaiciavimas(koeficientai, laipsniai);
            double[] tikslesniReziai = reziuSkiaciuokle.tikslesnisSaknuIntervalas();
            float formMinX = (float)(((int)reziuSkiaciuokle.RezisMinX(tikslesniReziai)) - 1);
            float formMaxX = (float)(((int)reziuSkiaciuokle.RezisMaxX(tikslesniReziai)) + 1);

            PreparareForm(formMinX, formMaxX, -10, 10);
            data.Clear();
            chart1.Series.Clear();

            drawGxFunction(-10, 10);

            List<double[]> saknuIntervalai = vykdytiSkenavima(reziuSkiaciuokle.RezisMinX(tikslesniReziai), reziuSkiaciuokle.RezisMaxX(tikslesniReziai), G);
            Series saknys = new Series("Šaknys");
            saknys.Color = Color.Red;
            saknys.ChartType = SeriesChartType.Point;

            richTextBox1.Clear();
            richTextBox1.AppendText(String.Format("{0,15} {1,15} {2,15}\n", "Iteracija", "X reiksme", "Y reiksme"));
            int iteracijos = 1;

            foreach (double[] intervalas in saknuIntervalai)
            {
                SaknuTikslinimas saknuTikslinimas = new SaknuTikslinimas(intervalas[0], intervalas[1], G, richTextBox1);
                double x = saknuTikslinimas.tikslintiKirstiniuMetodu(ref iteracijos);

                saknys.Points.AddXY(x, G(x));
            }

            chart1.Series.Add(saknys);
        }

        /// <summary>
        /// Skenavimo metodas F(x) mygtuko paspaudimas
        /// </summary>
        private void Button13_Click(object sender, EventArgs e)
        {
            ReziuSkaiciavimas reziuSkiaciuokle = new ReziuSkaiciavimas(koeficientai, laipsniai);
            double[] tikslesniReziai = reziuSkiaciuokle.tikslesnisSaknuIntervalas();
            float formMinX = (float)(((int)reziuSkiaciuokle.RezisMinX(tikslesniReziai)) - 1);
            float formMaxX = (float)(((int)reziuSkiaciuokle.RezisMaxX(tikslesniReziai)) + 1);

            PreparareForm(formMinX, formMaxX, -10, 10);
            data.Clear();
            drawFxFunction(formMinX, formMaxX);

            List<double[]> saknuIntervalai = vykdytiSkenavima(reziuSkiaciuokle.RezisMinX(tikslesniReziai), reziuSkiaciuokle.RezisMaxX(tikslesniReziai), F);
            Series saknys = new Series("Šaknys");
            saknys.Color = Color.Red;
            saknys.ChartType = SeriesChartType.Point;

            richTextBox1.Clear();
            richTextBox1.AppendText(String.Format("{0,15} {1,15} {2,15}\n", "Iteracija", "X reiksme", "Y reiksme"));
            int iteracijos = 0;

            foreach (double[] intervalas in saknuIntervalai)
            {
                SaknuTikslinimas saknuTikslinimas = new SaknuTikslinimas(intervalas[0], intervalas[1], F, richTextBox1);
                double x = saknuTikslinimas.tikslintiSkenavimoSuMazejanciuIntervaluMetodu(ref iteracijos);

                saknys.Points.AddXY(x, F(x));
            }
            

            chart1.Series.Add(saknys);
        }

        /// <summary>
        /// Skenavimo metodas G(x) mygtuko paspaudimas
        /// </summary>
        private void Button14_Click(object sender, EventArgs e)
        {
            ReziuSkaiciavimas reziuSkiaciuokle = new ReziuSkaiciavimas(koeficientai, laipsniai);
            double[] tikslesniReziai = reziuSkiaciuokle.tikslesnisSaknuIntervalas();
            float formMinX = (float)(((int)reziuSkiaciuokle.RezisMinX(tikslesniReziai)) - 1);
            float formMaxX = (float)(((int)reziuSkiaciuokle.RezisMaxX(tikslesniReziai)) + 1);

            PreparareForm(formMinX, formMaxX, -10, 10);
            data.Clear();
            chart1.Series.Clear();

            drawGxFunction(-10, 10);

            List<double[]> saknuIntervalai = vykdytiSkenavima(reziuSkiaciuokle.RezisMinX(tikslesniReziai), reziuSkiaciuokle.RezisMaxX(tikslesniReziai), G);
            Series saknys = new Series("Šaknys");
            saknys.Color = Color.Red;
            saknys.ChartType = SeriesChartType.Point;

            richTextBox1.Clear();
            richTextBox1.AppendText(String.Format("{0,15} {1,15} {2,15}\n", "Iteracija", "X reiksme", "Y reiksme"));
            int iteracijos = 0;
            foreach (double[] intervalas in saknuIntervalai)
            {
                SaknuTikslinimas saknuTikslinimas = new SaknuTikslinimas(intervalas[0], intervalas[1], G, richTextBox1);
                double x = saknuTikslinimas.tikslintiSkenavimoSuMazejanciuIntervaluMetodu(ref iteracijos);

                saknys.Points.AddXY(x, G(x));
            }

            chart1.Series.Add(saknys);
        }

        /// <summary>
        /// Greičio pokyčio grafikas mygtuko paspaudimas.
        /// </summary>
        private void Button15_Click(object sender, EventArgs e)
        {
            PreparareForm((float)Parametrai.GREITIS_MIN_X, (float)Parametrai.GREITIS_MAX_X, (float)Parametrai.GREITIS_MIN_Y, (float)Parametrai.GREITIS_MAX_Y);
            data.Clear();
            chart1.Series.Clear();

            Series Gx = chart1.Series.Add("V(c)");
            Gx.ChartType = SeriesChartType.Line;
            for (double x = Parametrai.GREITIS_MIN_X; x < Parametrai.GREITIS_MAX_X; x = x + 0.1)
            {
                Gx.Points.AddXY(x, V(x));
            }
            Gx.BorderWidth = 3;

            Series saknys = new Series("Šaknys");
            saknys.Color = Color.Red;
            saknys.ChartType = SeriesChartType.Point;

            List<double[]> saknuIntervalai = vykdytiSkenavima((float)Parametrai.GREITIS_MIN_X, (float)Parametrai.GREITIS_MAX_X, V);

            richTextBox1.Clear();
            richTextBox1.AppendText(String.Format("{0,15} {1,15} {2,15}\n", "Iteracija", "X reiksme", "Y reiksme"));
            int iteracijos = 1;
            double atsakymas = 0;
            foreach (double[] intervalas in saknuIntervalai) {
                SaknuTikslinimas saknuTikslinimas = new SaknuTikslinimas(intervalas[0], intervalas[1], V, richTextBox1);
                double x = saknuTikslinimas.tikslintiStyguMetodu(ref iteracijos);

                saknys.Points.AddXY(x, V(x));
                atsakymas = x;
            }

            richTextBox1.AppendText(String.Format("Atsakymas: {0,10:f5}", atsakymas));
            chart1.Series.Add(saknys);
        }


        // ---------------------------------------------- TIESINĖ ALGEBRA ----------------------------------------------

        /// <summary>
        /// Tiesine algebra (naudojama MathNet)
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            ClearForm();

            double[,] x = { { 1, 2, 3 }, { 3, 4, 5 }, { 6, 5, 8 } };
            // iš masyvo sugeneruoja matricą, is matricos išskiria eilutę - suformuoja vektorių
            Matrix<double> m = Matrix<double>.Build.DenseOfArray(x);
            Vector<double> v = m.Row(1);
            richTextBox1.AppendText("\nMatrica m:\n");
            richTextBox1.AppendText(m.ToString());

            richTextBox1.AppendText("\nVektorius v:\n");
            richTextBox1.AppendText(v.ToString());

            richTextBox1.AppendText("\ntranspose(m):\n");
            richTextBox1.AppendText(m.Transpose().ToString());

            Matrix<double> vm = v.ToRowMatrix();
            richTextBox1.AppendText("\nvm = v' - toRowMatrix()\n");
            richTextBox1.AppendText(vm.ToString());

            Vector<double> v1 = m * v;
            richTextBox1.AppendText("\nv1 = m * v\n");
            richTextBox1.AppendText(v1.ToString());
            richTextBox1.AppendText("\nmin(v1)\n");
            richTextBox1.AppendText(v1.Min().ToString());

            Matrix<double> m1 = m.Inverse();
            richTextBox1.AppendText("\ninverse(m)\n");
            richTextBox1.AppendText(m1.ToString());

            richTextBox1.AppendText("\ndet(m)\n");
            richTextBox1.AppendText(m.Determinant().ToString());

            // you must add reference to assembly system.Numerics
            Evd<double> eigenv = m.Evd();
            richTextBox1.AppendText("\neigenvalues(m)\n");
            richTextBox1.AppendText(eigenv.EigenValues.ToString());
            
            LU<double> LUanswer = m.LU();
            richTextBox1.AppendText("\nMatricos M LU skaida\n");
            richTextBox1.AppendText("\nMatrica L:\n");
            richTextBox1.AppendText(LUanswer.L.ToString());
            richTextBox1.AppendText("\nMatrica U:\n");
            richTextBox1.AppendText(LUanswer.U.ToString());
            
            QR<double> QRanswer = m.QR();
            richTextBox1.AppendText("\nMatricos M QR skaida\n");
            richTextBox1.AppendText("\nMatrica Q:\n");
            richTextBox1.AppendText(QRanswer.Q.ToString());
            richTextBox1.AppendText("\nMatrica R:\n");
            richTextBox1.AppendText(QRanswer.R.ToString());

            Vector<double> v3 = m.Solve(v);
            richTextBox1.AppendText("\nm*v3 = v sprendziama QR metodu\n");
            richTextBox1.AppendText(v3.ToString());
            richTextBox1.AppendText("Patikrinimas\n");
            richTextBox1.AppendText((m * v3 - v).ToString());
            
        }
        

        // ---------------------------------------------- KITI METODAI ----------------------------------------------

        /// <summary>
        /// Uždaroma programa
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        /// <summary>
        /// Išvalomas grafikas ir consolė
        /// </summary>
        private void button4_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        
         
        public void ClearForm()
        {
            richTextBox1.Clear(); // isvalomas richTextBox1
            // sustabdomi timeriai jei tokiu yra
            foreach (var timer in Timerlist)
            {
                timer.Stop();
            }

            // isvalomos visos nubreztos kreives
            chart1.Series.Clear();
        }
    }
}
