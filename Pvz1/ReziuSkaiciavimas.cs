﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pvz1
{
    class ReziuSkaiciavimas
    {
        private List<double> koeficientai;
        private List<int> laipsniai;

        public float RezisMinX(double[] reziai) { return  (float) reziai[0]; }

        public float RezisMaxX(double[] reziai) { return (float) reziai[1]; }

        public ReziuSkaiciavimas(List<double> koeficientai, List<int> laipsniai) {
            this.koeficientai = koeficientai;
            this.laipsniai = laipsniai;
        }

        public double[] grubusSaknuIntervalas()
        {
            List<double> koef = koeficientai.GetRange(1, koeficientai.Count - 1);
            double maxKoef = 0;

            foreach (double elem in koef)
            {
                if (maxKoef < Math.Abs(elem))
                {
                    maxKoef = Math.Abs(elem);
                }
            }

            double riba = 1 + (maxKoef / koeficientai[0]);
            if (riba < 0)
                return new double[] { riba, -riba };
            else
                return new double[] { -riba, riba };
        }

        public double[] tikslesnisSaknuIntervalas()
        {
            // Teigiamoms saknims
            double virsutinisRezis = tikslusRezis(koeficientai.ToArray());

            // Neigiamoms saknims
            double apatinisRezis;
            if ((koeficientai.Count() - 1) % 2 == 0)
            {
                double[] subKoef = Helper.SubArray(koeficientai.ToArray(), 0, koeficientai.Count());
                for (int i = 0; i < subKoef.Count(); i++)
                {
                    subKoef[i] = -1 * subKoef[i];
                }

                if (subKoef[0] < 0)
                {
                    subKoef[0] = -1 * subKoef[0];
                }

                apatinisRezis = tikslusRezis(subKoef);
            }
            else
            {
                double[] subKoef = Helper.SubArray(koeficientai.ToArray(), 0, koeficientai.Count());
                for (int i = 0; i < koeficientai.Count(); i++)
                {
                    if (laipsniai[i] % 2 != 0)
                    {
                        subKoef[i] = -1 * subKoef[i];
                    }
                }

                if (subKoef[0] < 0)
                {
                    subKoef[0] = -1 * subKoef[0];
                }

                apatinisRezis = tikslusRezis(subKoef);
            }

            double[] grubus = grubusSaknuIntervalas();

            if (apatinisRezis > RezisMinX(grubus)) {
                apatinisRezis = RezisMinX(grubus);
            }

            if (virsutinisRezis > RezisMaxX(grubus)) {
                virsutinisRezis = RezisMaxX(grubus);
            }

            return new double[] { apatinisRezis, virsutinisRezis };
        }

        private double tikslusRezis(double[] koef)
        {
            List<double> neigKoef = new List<double>();
            List<int> neigIndeksai = new List<int>();

            for (int i = 1; i < koef.Count(); i++)
            {
                if (koef[i] < 0)
                {
                    neigKoef.Add(koef[i]);
                    neigIndeksai.Add(koef.Count() - i - 1);
                }
            }

            double maxKoef = 0;
            foreach (double elem in neigKoef)
            {
                if (maxKoef < Math.Abs(elem))
                {
                    maxKoef = Math.Abs(elem);
                }
            }

            int k = koef.Count() - 1 - neigIndeksai.Max();
            return 1.0 + Math.Pow((maxKoef / koef[0]), 1.0/k);
        }

    }
}
