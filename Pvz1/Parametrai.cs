﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pvz1
{
    static class Parametrai
    {

        public static double SKENAVIMO_INTERVALAS = 0.1;

        public static double NORIMAS_TIKSLUMAS = 0.01;

        public static int MAKSIMALUS_ITERACIJU_SKAICIUS = 500;

        public static double KIRSTINIU_METODO_ANTI_KONVERGAVIMO_KOEFICIENTAS = 0.001;

        // Uzduotis 3

        public static double GREITIS_MIN_X = -2;
        public static double GREITIS_MAX_X = 5;
        public static double GREITIS_MIN_Y = -80;
        public static double GREITIS_MAX_Y = 20;

    }
}
